---
title: "Belajar Elixir Intro"
date: 2019-08-13T15:27:16+07:00
draft: false
---

Tulisan ini ane bikin sebagai rujukan waktu ane belajar programming dengan Elixir. sengaja ane tulis dengan alasan kali aja ada teman-teman yang baru mulai belajar Elixir dan bingung harus mulai dari mana.

Ane pake Macbook pro dan manjaro linux saat belajar elixir, karena elixir masih lumayan baru, release versionnya masih kenceng bgt tuh, makanya untuk ngakalin ginian, kita butuh juga yang namanya version manager, nah beruntung nya sekarang udah ada yang namanya "asdf", dengan asdf kita tinggal ketik:

```bash
asdf plugin-add erlang
asdf install erlang 22.0.4 && asdf global erlang 22.0.4
asdf plugin-add elixir
asdf install elixir 1.8.2-otp-22 && asdf global elixir 1.8.2-otp-22

```

asdf tuh single version manager yg support banyak sekali bahasa pemrograman juga tooling. kalo jaman dulukan ada node version manager, ruby version manager dll, belum lg tiap bahasa punya lebih dari satu version manager, ah ribet dah pokok nya.

nih contoh asdf current yang saya pakai:

```bash
asdf current

```

```bash
crystal        0.29.0   (set by /home/rhidayat/.tool-versions)
dart           2.4.0    (set by /home/rhidayat/.tool-versions)
draft          No version set for draft; please run `asdf <global | local> draft <version>`
elixir         1.8.2-otp-22 (set by /home/rhidayat/.tool-versions)
erlang         22.0.4   (set by /home/rhidayat/.tool-versions)
flutter        1.7.8+hotfix.3-stable (set by /home/rhidayat/.tool-versions)
gohugo         0.55.6   (set by /home/rhidayat/.tool-versions)
golang         1.12.7   (set by /home/rhidayat/.tool-versions)
helm           2.13.1   (set by /home/rhidayat/.tool-versions)
java           openjdk-10.0.2 (set by /home/rhidayat/.tool-versions)
julia          1.1.1    (set by /home/rhidayat/.tool-versions)
kotlin         1.3.41   (set by /home/rhidayat/.tool-versions)
kubectl        1.14.0   (set by /home/rhidayat/.tool-versions)
minikube       1.2.0    (set by /home/rhidayat/.tool-versions)
nodejs         10.12.0  (set by /home/rhidayat/.tool-versions)
version 10.9 is not installed for postgres
postgres
python         3.7.4    (set by /home/rhidayat/.tool-versions)
ruby           2.5.5    (set by /home/rhidayat/.tool-versions)
rust           stable   (set by /home/rhidayat/.tool-versions)
terraform      0.11.14  (set by /home/rhidayat/.tool-versions)
yarn           1.17.3   (set by /home/rhidayat/.tool-versions)
```

eh buset banyak banget bang!!!, yah namanya juga devops eh sysadmin :)
kalo lu kerja jadi sysadmin atau devops atau SRE atau infrastructure engineer ya gt dah kudu tau banyak bahasa, kan lu yg jadi babysitter buat aplikasi yang d tulis sama developer2 itu. kadang nih ya mereka sok2 an pake berbagai macam bahasa, ga tau apa alasannya, mungkin biar keren kali yah, padahal mah buat sysadminnya makin banyak bahasanya makin pusing wkwkwk. biar lu ga kagok yah minimal lu tau cara nginstallnya, multi versi, lalu lu tau dikit2 cara nulis kode dalam bahasa itu, lu tau cara baca variable config dari mana datangnya, ini banyak lu kerjain ketika migrasi dari model virtual machine ke docker juga kubernetes.

teks editor yang ane pake ya standar aja sih, visual studio code yg opensource version :)
tinggal install plugin yg support Elixir, formatter, docs dll, googling aja ya, tujuannya supaya lu nyaman kerja sama elixir aja.

setelah install ini itu, setup ini itu, saat nya kita masuk ke mode REPL elixir, ketik:

```bash

[rhidayat@rhidayat-pc rhidayat]$ iex
Erlang/OTP 22 [erts-10.4.3] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe]

Interactive Elixir (1.8.2) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> 

```

coba ikutin link dibawah ini:

https://learnxinyminutes.com/docs/elixir/ 

https://elixir-lang.org/getting-started/introduction.html


Kalo udah baca link2 itu, singkat aja, gausah sampe detil, detilnya mah kan dari blog ini nantinya, bgmana sih hehehe. ane bakalan ngajak pelan-pelan masuk ke Elixir, karena ane udah baca buku-buku Elixir jadi kepikiran lah bagaimana seharusnya teman-teman mulai biar lancar jaya ga kaya ane yang kaya masuk hutan belantara, jatuh sana jatuh sini, jungkir balik kadang-kadang sampe jadi males jalan lagi, ah lebay nih si om curhatnya hahaha.

conclusion:

untuk mudahin nginstal-nginstall please pake asdf, link nya ada disini https://asdf-vm.com/#/

setup visual studio code dan plugin-pluginnya, khusus buat Elixir

baca sepintas soal Elixir di

https://learnxinyminutes.com/docs/elixir/ dan di 

https://elixir-lang.org/getting-started/introduction.html

